#!/bin/bash
newrelic-admin run-program uwsgi studio/conf/uwsgi.ini --http 0.0.0.0:8080
celery -A studio beat -l INFO --detach
celery -A studio worker -l  INFO --concurrency=2 --max-tasks-per-child=2