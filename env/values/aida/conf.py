CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "aida-alpha2",
        "PORT": "5432",
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "aida"
    },
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://e0a7067bcae742b1a17871ac5b8423b5@sentry.livspace.com/79"
    }
}

GATEWAY = {
    "HOST": "axle",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        "HOST": "launchpad-backend",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/launchpad'
        }
    },
    "BOUNCER": {
        'HOST': 'api.alpha2.livspace.com/bouncer',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/bouncer'
        }
    }
}
